#ifndef FM_H
#define FM_H

#include "FS.h"
#include "SPIFFS.h"

class FileManager
{
public:
    FileManager(void);

    void begin();

    void listDir(const char *dirname, uint8_t levels);
    void readFile(const char *path);
    void writeFile(const char *path, const char *message);
    void appendFile(const char *path, const char *message);
    void renameFile(const char *path1, const char *path2);
    void deleteFile(const char *path);
    void testFileIO(const char *path);
};

#endif